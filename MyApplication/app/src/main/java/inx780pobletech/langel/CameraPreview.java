package inx780pobletech.langel;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.ArrayList;

import inx780pobletech.langel.ldws.ImageOps;
import inx780pobletech.langel.ldws.OverlayControl;

/** A basic Camera preview class */
@SuppressWarnings("deprecation")
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {
    private String TAG = "L'ANGEL-CAM-PREVIEW";
    public static int AFPS = 0;
    public static int avgAFPS = 0;
    public static ArrayList<Integer> averageAFPS_set = new ArrayList<>();
    //
    private long startTime = 0;
    private OverlayControl overlay;
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Camera.PreviewCallback mPreviewCallback;
    public CameraPreview(Context context, Camera camera, OverlayControl overlay) {
        super(context);
        mCamera = camera;
        this.mPreviewCallback = this;
        this.overlay = overlay;
        //Install a SurfaceHolder.Callback so we get notified when the underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
    }
    @Override public void onDraw(Canvas canvas) {

    }
    @Override public void surfaceCreated(SurfaceHolder holder) {
        //The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewCallback(mPreviewCallback);
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }
    @Override public void surfaceDestroyed(SurfaceHolder holder) {
        //empty. Take care of releasing the Camera preview in your activity.
    }
    @Override public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        //If your preview can change or rotate, take care of those events here.
        //Make sure to stop the preview before resizing or reformatting it.
        if (mHolder.getSurface() == null) return;   // preview surface does not exist
        //stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            e.printStackTrace();
        }
        //set preview size and make any resize, rotate or reformatting changes here
        //start preview with new settings
        try {
            mCamera.setPreviewCallback(mPreviewCallback);
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }
    /**Main thread for getting the camera preview bitmap*/
    @Override public void onPreviewFrame(byte[] data, Camera camera) {
        synchronized (this) {
            try {
                //Register image////////////////////////////////////////////////////////////////////
                Camera.Parameters parameters = camera.getParameters();
                int width = parameters.getPreviewSize().width;
                int height = parameters.getPreviewSize().height;
                //Log.d(TAG, "Byte data size " + data.length);

                new ImageOps(data, width, height, true);
                //new ImageOps(data, width, height, false);
                if(System.currentTimeMillis() - startTime > 1000) {
                    averageAFPS_set.add(AFPS);
                    if(averageAFPS_set.size() > 5) {
                        for(int i = 0; i < averageAFPS_set.size(); i++) {
                            avgAFPS += averageAFPS_set.get(i);
                        }
                        avgAFPS /= averageAFPS_set.size();
                        averageAFPS_set.clear();
                    }
                    AFPS = 0;
                    startTime = System.currentTimeMillis();
                } else AFPS++;
                //Update overlay////////////////////////////////////////////////////////////////////
                overlay.updateOverlay(width, height, CameraActivity.roll);
                //Check Bounds//////////////////////////////////////////////////////////////////////
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}