package inx780pobletech.langel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void launchCamera(View view) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }
    public void configureOverlay(View view) {
        Intent intent = new Intent(this, ConfigureActivity.class);
        startActivity(intent);
    }
    public void exitApp(View view) {
        finish();
    }
}
