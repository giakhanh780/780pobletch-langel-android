package inx780pobletech.langel.ldws;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class ImageOps implements Runnable {
    //Fixed/High Level Constants////////////////////////////////////////////////////////////////////
    private static final String TAG = "IMAGE-OPS";
    private static int instances = 0;
    //Instance fields///////////////////////////////////////////////////////////////////////////////
    private Thread thread;
    private long startTime;
    private boolean isWorking = false;
    //Image Related Instance Fields/////////////////////////////////////////////////////////////////
    private Bitmap bImage;
    private byte[] bData, rData;
    private int width = 0, height = 0;
    private boolean leftSide = false;

    public ImageOps(byte[] data, int width, int height, boolean leftSide) {
        //bData = Arrays.copyOf(data, data.length);
        this.rData = data;
        this.width = width;
        this.height = height;
        this.leftSide = leftSide;
        startOperations();
    }
    /**Thread Operations & Control*************************************************************************************/
    public void startOperations() {
        thread = new Thread(this);
        thread.start();
        instances += 1;
    }
    public void stopOperations() {
        if(!Thread.interrupted()) {
            thread.interrupt();
            thread = null;
            instances -= 1;
        } else {
            Log.d(TAG, "Thread has already stopped");
        }
    }
    @Override public void run() {
        isWorking = true;
        startTime = System.currentTimeMillis();
        bData = Arrays.copyOf(rData, rData.length);
        bImage = convertYuvImageToRgb();
        if(bImage != null) {
            int Y_HEIGHT_CONST = (bImage.getHeight()/3)*2;
            int SCAN_WIDTH = 30;
            //scanForLane(bImage.getWidth()/2, bImage.getWidth()/2 - (bImage.getWidth()/4), Y_HEIGHT_CONST, true);
            //scanForLane(bImage.getWidth()/2, bImage.getWidth()/2 + (bImage.getWidth()/4), Y_HEIGHT_CONST, false);
            scaneForLane(Y_HEIGHT_CONST, SCAN_WIDTH, true);
            scaneForLane(Y_HEIGHT_CONST, SCAN_WIDTH, false);
        }
        Log.d(TAG, "ImageOps operation time - " + (System.currentTimeMillis() - startTime));
        //END
        bImage.recycle();
        isWorking = false;
        instances -= 1;
    }
    public boolean isWorking() {
        return isWorking;
    }
    public static int getInstances() {
        return instances;
    }
    /**Image Operations***********************************************************************************************/
    private Bitmap convertYuvImageToRgb() {
        int downSample = 1;
        YuvImage yuvImage = new YuvImage(bData, ImageFormat.NV21, width, height, null);

        Bitmap rgbImage;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, width, height), 0, out);
        byte[] imageBytes = out.toByteArray();

        BitmapFactory.Options opt;
        opt = new BitmapFactory.Options();
        opt.inSampleSize = downSample;

        rgbImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, opt);
        return rgbImage;
    }
    private boolean scanForLane(int startX, int endX, int yLevel, boolean leftSide) {
        if(leftSide) {
            int highestWhiteV = 0, highestWhiteX = 0, current;
            for(int x = startX; x >= endX; x--) {
                current = analyzeWhitenessHC(x, yLevel, LDWSConfig.CONTRAST_CONST);
                if(current > highestWhiteV) {
                    highestWhiteV = current;
                    highestWhiteX = x;
                }
            }
            if(highestWhiteV > LDWSConfig.LANE_ALPHA_THRESHOLD) {
                LDWSConfig.lane_left_x1 = highestWhiteX;
                LDWSConfig.whiteLeftFound = highestWhiteV;
                return true;
            }
        } else {
            int highestWhiteV = 0, highestWhiteX = 0, current;
            for(int x = startX; x <= endX; x++) {
                current = analyzeWhitenessHC(x, yLevel, LDWSConfig.CONTRAST_CONST);
                if(current > highestWhiteV) {
                    highestWhiteV = current;
                    highestWhiteX = x;
                }
            }
            if(highestWhiteV > LDWSConfig.LANE_ALPHA_THRESHOLD) {
                LDWSConfig.lane_right_x1 = highestWhiteX;
                LDWSConfig.whiteRightFound = highestWhiteV;
                return true;
            }
        }
        return false;
    }
    private boolean scaneForLane(int yLevel, int scanWidth, boolean leftSide) {
        int leftBoundX = (bImage.getWidth()/2) - (LDWSConfig.getFullVehicleWidthInches()/2);
        int rightBoundX = (bImage.getWidth()/2) + (LDWSConfig.getFullVehicleWidthInches()/2);
        int highestWhiteV = 0, highestWhiteX = 0, current;
        if(leftSide) {
            for(int x = leftBoundX + scanWidth; x >= leftBoundX - scanWidth; x--) {
                current = analyzeWhitenessHC(x, yLevel, LDWSConfig.CONTRAST_CONST);
                if(current > highestWhiteV) {
                    highestWhiteV = current;
                    highestWhiteX = x;
                }
            }
            if(highestWhiteV > LDWSConfig.LANE_ALPHA_THRESHOLD) {
                LDWSConfig.lane_left_x1 = highestWhiteX;
                LDWSConfig.whiteLeftFound = highestWhiteV;
                return true;
            } else {
                LDWSConfig.lane_left_x1 = 0;
                LDWSConfig.whiteLeftFound = highestWhiteV;
            }
        } else {
            for(int x = rightBoundX - scanWidth; x < rightBoundX + scanWidth; x++) {
                current = analyzeWhitenessHC(x, yLevel, LDWSConfig.CONTRAST_CONST);
                if(current > highestWhiteV) {
                    highestWhiteV = current;
                    highestWhiteX = x;
                }
            }
            if(highestWhiteV > LDWSConfig.LANE_ALPHA_THRESHOLD) {
                LDWSConfig.lane_right_x1 = highestWhiteX;
                LDWSConfig.whiteRightFound = highestWhiteV;
                return true;
            } else {
                LDWSConfig.lane_right_x1 = width;
                LDWSConfig.whiteRightFound = highestWhiteV;
            }
        }
        return false;
    }
    private void analyzePixelForColor(int x, int y) {
        int pixel = bImage.getPixel(x, y);
        LDWSConfig.redValue = Color.red(pixel);
        LDWSConfig.greenValue = Color.green(pixel);
        LDWSConfig.blueValue = Color.blue(pixel);
        LDWSConfig.whiteValue = (int)(0.299*LDWSConfig.redValue + 0.587*LDWSConfig.greenValue + 0.0722*LDWSConfig.blueValue);
    }
    private int analyzeWhiteness(int x, int y) {
        int pixel = bImage.getPixel(x, y);
        return (int)(0.299*Color.red(pixel) + 0.587*Color.green(pixel) + 0.0722*Color.blue(pixel));
    }
    private int analyzeWhitenessHC(int x, int y, int contrastVal) {
        double contrast = Math.pow((100 + contrastVal) / 100, 2);
        // get pixel color
        int pixel = bImage.getPixel(x, y);

        // apply filter contrast for every channel R, G, B
        int red = Color.red(pixel);
        red = (int)(((((red / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if(red < 0) red = 0;
        else if(red > 255) red = 255;

        int green = Color.green(pixel);
        green = (int)(((((green / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if(green < 0) green = 0;
        else if(green > 255) green = 255;

        int blue = Color.blue(pixel);
        blue = (int)(((((blue / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if(blue < 0) blue = 0;
        else if(blue > 255) blue = 255;

        return (int)(0.299*red + 0.587*green + 0.0722*blue);
    }
}
