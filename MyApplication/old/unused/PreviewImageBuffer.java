package inx780pobletech.langel.unused;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

@Deprecated
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class PreviewImageBuffer implements Cloneable {
    private Bitmap bitmap;
    private boolean available = false;
    public PreviewImageBuffer(YuvImage yuvImage, int width, int height) {
        bitmap = convertYuvImageToRgb(yuvImage, width, height, 1);
        available = true;
    }
    public boolean isAvailable() {
        return available;
    }
    private Bitmap convertYuvImageToRgb(YuvImage yuvImage, int width, int height, int downSample) {
        Bitmap rgbImage;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, width, height), 0, out);
        byte[] imageBytes = out.toByteArray();

        BitmapFactory.Options opt;
        opt = new BitmapFactory.Options();
        opt.inSampleSize = downSample;

        rgbImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, opt);
        return rgbImage;
    }
}
